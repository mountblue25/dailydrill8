const products = [{
    shampoo: {
        price: "$50",
        quantity: 4
    },
    "Hair-oil": {
        price: "$40",
        quantity: 2,
        sealed: true
    },
    comb: {
        price: "$12",
        quantity: 1
    },
    utensils: [
        {
            spoons: { quantity: 2, price: "$8" }
        }, {
            glasses: { quantity: 1, price: "$70", type: "fragile" }
        }, {
            cooker: { quantity: 4, price: "$900" }
        }
    ],
    watch: {
        price: "$800",
        quantity: 1,
        type: "fragile"
    }
}]


//Problem1 - Find all the items with price more than $65.
let output = {}
let result1 = products.map((obj,) => {
    const items = Object.keys(obj)
    items.map((eachItem) => {
        if (obj[eachItem].price) {
            if (Number(obj[eachItem].price.replace("$", "")) > 65) {
                output[eachItem] = obj[eachItem]
            }
        }

    })
})
console.log(output)
